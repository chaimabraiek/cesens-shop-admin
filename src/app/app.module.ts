import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AddSensorComponent } from './add-sensor/add-sensor.component';
import { AddPackComponent } from './add-pack/add-pack.component';
import { AddStationComponent } from './add-station/add-station.component';
import { ListPacksComponent } from './list-packs/list-packs.component';
import { AppRoutingModule } from './app-routing.module';

import { HttpClientModule } from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SweetAlert2Module} from "@sweetalert2/ngx-sweetalert2";
import { ListSensorsComponent } from './list-sensors/list-sensors.component';

@NgModule({
  declarations: [
    AppComponent,
    AddSensorComponent,
    AddPackComponent,
    AddStationComponent,
    ListPacksComponent,
    ListSensorsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
