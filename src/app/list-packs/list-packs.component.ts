import { Component, OnInit } from '@angular/core';
import {estacion} from '../models/estacion';
import {PackService} from '../service/pack.service';
import Swal from "sweetalert2";

@Component({
  selector: 'app-list-packs',
  templateUrl: './list-packs.component.html',
  styleUrls: ['./list-packs.component.css']
})
export class ListPacksComponent implements OnInit {

  constructor(private packService: PackService) { }

  products: estacion[] = [];
  stationEn : estacion = new estacion();

  ngOnInit(): void {
    this.packService.getPacks().subscribe((data: estacion[]) => {
      console.log(data);
      this.products = data;
    });
    console.log(localStorage.getItem('stationEn'));
    console.log(localStorage.getItem('stationEs'));
  }

  deletePack(id){
    Swal.fire({
      title: 'Estas seguro?',
      text: "¡No podrás revertir esto!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: '¡Si, bórralo!'
    }).then((result) => {
      if (result.value) {
        this.packService.deleteStation(id).subscribe(()=>console.log('deleted'));
        Swal.fire(
          '¡Eliminado!',
          'Su paquete ha sido eliminado.',
          'success'
        );
        //window.location.reload();
      }
    }).then(()=> window.location.reload());
  }

  editPack(station , id, stationEn , idEn){
    this.packService.editStation(station, id).subscribe(()=>{
      console.log('success');
      Swal.fire(
        'Modificado!',
        'Su paquete ha sido modificado.',
        'success'
      )
    });
    this.packService.editStationEn(stationEn, idEn).subscribe(()=>{
      console.log('en success')
    })
  }

  getEnStation(id){
    this.packService.getEnglishStationFromEs(id).subscribe((data)=>{
      console.log(data);
      this.stationEn = data;
    });
  }

}
