import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import {estacion} from '../models/estacion';
import {sensor} from '../models/sensor';


@Injectable({
  providedIn: 'root'
})
export class PackService {

   private urlEs = 'http://192.168.1.217:98/shop/estaciones/';
   private urlEn = 'http://192.168.1.217:98/shop/estaciones_en/';
   private urlSensorsEs = 'http://192.168.1.217:98/shop/sensors/';
   private urlSensorsEn = 'http://192.168.1.217:98/shop/sensors_en/';
   httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  constructor(private httpClient: HttpClient) { }

  // get all packs in spanish
  getPacks(): Observable<estacion[]> {
    return this.httpClient.get<estacion[]>(this.urlEs);
  }

  getOneStationById(id): Observable<estacion> {
    return this.httpClient.get<estacion>(`${this.urlEs}` + id);
  }

  // get all packs in english
  getPacksEn(): Observable<estacion[]> {
    return this.httpClient.get<estacion[]>(this.urlEn);
  }

  // get a pack's sensors in spanish
  getSensorsEs(id): Observable<sensor[]> {
    console.log(id);
    return this.httpClient.get<sensor[]>(`${this.urlEs}`+`1/` + id);
  }

  // get a pack's sensors in english
  getSensorsEn(id): Observable<sensor[]> {
    return this.httpClient.get<sensor[]>(`${this.urlEn}`+`1/` + id);
  }

  // add a sensor in spanish
  async addSensor(sensor: Object){
    console.log(sensor);
    return await this.httpClient.post(`${this.urlSensorsEs}`, sensor).toPromise();
  }

  // add a sensor in english
  async addSensorEn(sensor: Object) {
    console.log(sensor);
    return await this.httpClient.post(`${this.urlSensorsEn}`, sensor).toPromise();
  }

  // add the english sensor id to the spanish sensor table
  addEnSensorToEsSensor(es,en){
    return this.httpClient.put(`${this.urlSensorsEs}` +`edit/`+ es +`/`+ en, null);
  }

  // add a station in spanish
  async addStationEs(estacion: Object) {
    console.log(estacion);
    return await this.httpClient.post(`${this.urlEs}`, estacion).toPromise();
  }

  // add a station in english
  async addStationEn(estacion: Object) {
    console.log(estacion);
    return await this.httpClient.post(`${this.urlEn}`, estacion).toPromise();
  }

  // add the english station to its translation in spanish
  addEnStationToEsStation(es,en) {
    return this.httpClient.put(`${this.urlEs}` +`edit/`+ es +`/`+ en, null);
  }

  // affect the sensor to the station in estacion_sensor table : spanish
  addSensorAndStationEs(station,sensor){
    return this.httpClient.post(`${this.urlEs}` + station +`/`+ sensor, null);
  }

  // affect the sensor to the station in estacion_sensor table : english
  addSensorAndStationEn(station,sensor){
    return this.httpClient.post(`${this.urlEn}` + station +`/`+ sensor, null);
  }

  deleteStation(id){
    return this.httpClient.delete(`${this.urlEs}`  +`delete/`+ id)
  }

  deleteSensor(id){
    return this.httpClient.delete(`${this.urlSensorsEs}` +`delete/` + id)
  }

  editStation(station, id){
    return this.httpClient.put(`${this.urlEs}`  +`edit/`+ id, station)
  }

  editStationEn(station, id){
    return this.httpClient.put(`${this.urlEn}`  +`edit/`+ id, station)
  }

  getEnglishStationFromEs(id): Observable<estacion>{
    return this.httpClient.get<estacion>(`${this.urlEs}`  +`en/`+ id);
  }

  editSensor(sensor, id){
    return this.httpClient.put(`${this.urlSensorsEs}`  +`edit/`+ id, sensor)
  }

  editSensorEn(sensor, id){
    return this.httpClient.put(`${this.urlSensorsEn}`  +`edit/`+ id, sensor)
  }

  getEnglishSensorFromEs(id): Observable<sensor>{
    return this.httpClient.get<sensor>(`${this.urlSensorsEs}`  +`en/`+ id);
  }
}
