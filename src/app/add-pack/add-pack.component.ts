import {Component, ComponentFactoryResolver, ElementRef, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {PackService} from '../service/pack.service';
import {estacion} from '../models/estacion';
import Swal from "sweetalert2";
import {sensor} from '../models/sensor';
declare const $: any;

@Component({
  selector: 'app-add-pack',
  templateUrl: './add-pack.component.html',
  styleUrls: ['./add-pack.component.css']
})
export class AddPackComponent implements OnInit {

  @ViewChild('container', {read: ViewContainerRef}) container: ViewContainerRef;

  constructor(private packService: PackService, private componentFactoryResolver: ComponentFactoryResolver) { }

  // Keep track of list of generated components for removal purposes
  components = [];

  station : estacion = new estacion();
  sensor: sensor = new sensor();
  sensorEn: sensor = new sensor();
  products: estacion[] = [];
  sensorEnId;

  ngOnInit() {

    this.packService.getPacks().subscribe((data: estacion[]) => {
      console.log(data);
      this.products = data;
    });

    this.packService.getSensorsEs(6).subscribe((data: sensor[]) => {console.log(data)})
  }

  async addSensor(){
    await this.packService.addSensor(this.sensor).then((data: sensor)=>{
        console.log(data);
        Swal.fire({
          title: 'Exito!',
          text: 'Sensor agregado con éxito',
          icon: 'success',
          confirmButtonText: 'Ok'
        });

        this.sensorEn.sensorEsId = data.id;
        this.addSensorEn().then((data2: sensor)=>{
            this.sensorEnId = data2.id;
            console.log(this.sensorEnId);
            this.addEnglishSensorToSpanishSensor(data.id, this.sensorEnId);
            this.packService.addSensorAndStationEs(localStorage.getItem('stationEs'),data.id).subscribe((d)=>console.log(d));
            this.packService.addSensorAndStationEn(localStorage.getItem('stationEn'),this.sensorEnId).subscribe((d)=>console.log(d));
          }
          , error1 => console.log(error1));
      }
      , error1 => console.log(error1));
  }

  async addSensorEn() {
    this.sensorEn.type = this.sensor.type;
    this.sensorEn.quantity = this.sensor.quantity;
    return await this.packService.addSensorEn(this.sensorEn);
  }

  addEnglishSensorToSpanishSensor(es, en){
    this.packService.addEnSensorToEsSensor(es,en).subscribe((data)=> console.log(data));
  }

  addDiv() {
    //var div= this.elementRef.nativeElement.querySelector('#newDiv');
    //div.insertAdjacentElement('beforeend','<p> test </p>');
   /* let a = document.getElementById('newDiv');
    let p = document.createElement('p');
    p.append('test test');
    a.append(p);*/
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(AddPackComponent);
    const component = this.container.createComponent(componentFactory);
    // Push the component so that we can keep track of which components are created
    this.components.push(component);
    console.log(this.components);
  }
}
