
export class sensor {
  id: number;
  name: string;
  type: string;
  description: string;
  quantity: number;
  photo: string;
  sensor_en_id: number;
  sensorEsId: number;
}
