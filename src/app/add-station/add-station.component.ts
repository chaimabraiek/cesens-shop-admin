import {Component, ComponentFactoryResolver, ElementRef, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {PackService} from '../service/pack.service';
import {estacion} from '../models/estacion';
import Swal from "sweetalert2";
import {AddPackComponent} from '../add-pack/add-pack.component';
import {Router} from '@angular/router';
import {createElementCssSelector} from '@angular/compiler';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-add-station',
  templateUrl: './add-station.component.html',
  styleUrls: ['./add-station.component.css']
})
export class AddStationComponent implements OnInit {

  @ViewChild('container', {read: ViewContainerRef}) container: ViewContainerRef;

  constructor(private packService: PackService, private componentFactoryResolver: ComponentFactoryResolver, private http: HttpClient) { }

  // Keep track of list of generated components for removal purposes
  components = [];

  station : estacion = new estacion();
  stationEn : estacion = new estacion();
  products: estacion[] = [];
  stationEnId;

  ngOnInit(): void {
  }

  async addStation(){
    await this.packService.addStationEs(this.station).then((data: estacion)=>{
        console.log(data);
        Swal.fire({
          title: 'Exito!',
          text: 'Estacion en Español agregada con éxito',
          icon: 'success',
          confirmButtonText: 'Ok'
        });

        this.stationEn.estacionId = data.id;
        this.addStationEn().then((data2: estacion)=>{
            this.stationEnId = data2.id;
            console.log(this.stationEnId);
            this.addEnglishStationToSpanishStation(data.id, this.stationEnId);
            localStorage.setItem('stationEn', this.stationEnId );
            localStorage.setItem('stationEs', data.id.toString() );
          }
          , error1 => console.log(error1));
      }
      , error1 => console.log(error1));
  }

  async addStationEn() {
    this.stationEn.price = this.station.price;
    this.stationEn.garantia = this.station.garantia;
    this.stationEn.software = this.station.software;
    return await this.packService.addStationEn(this.stationEn);
  }

  addEnglishStationToSpanishStation(es, en){
    this.packService.addEnStationToEsStation(es,en).subscribe((data)=> console.log(data));
  }

  addDiv() {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(AddPackComponent);
    const component = this.container.createComponent(componentFactory);
    // Push the component so that we can keep track of which components are created
    this.components.push(component);
    console.log(this.components);
  }

  public imagePath;
  url: string | ArrayBuffer;
  onSelectFile(event) { // called each time file input changes
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      this.imagePath = event.target.files;
      const formData = new FormData();
      for (const file of this.imagePath) {
        formData.append('image', file, file.name);
      }
      const headers = new HttpHeaders();
      headers.append('Content-Type', 'multipart/form-data');
      headers.append('Accept', 'application/json');
      // @ts-ignore
      this.http.post('http://localhost/mypage.php', formData).subscribe( headers, console.log(file.name) );

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => { // called once readAsDataURL is completed
        this.url = event.target.result;

      }
    }
}
}
