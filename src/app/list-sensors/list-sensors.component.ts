import { Component, OnInit } from '@angular/core';
import {PackService} from '../service/pack.service';
import {sensor} from '../models/sensor';
import {ActivatedRoute} from '@angular/router';
import Swal from "sweetalert2";
import {estacion} from '../models/estacion';

@Component({
  selector: 'app-list-sensors',
  templateUrl: './list-sensors.component.html',
  styleUrls: ['./list-sensors.component.css']
})
export class ListSensorsComponent implements OnInit {

  constructor(private packService: PackService, private readonly route: ActivatedRoute) { }

  idStation : number;
  sensors : sensor[] = [];
  sensorEn : sensor = new sensor();
  station : estacion = new estacion();
  stationEn : estacion = new estacion();

  ngOnInit(): void {
    this.idStation =+ this.route.snapshot.paramMap.get("id");
    //console.log(this.route.snapshot.paramMap.get("id"));
    //console.log(typeof (this.idStation));
    this.packService.getSensorsEs(this.idStation).subscribe((data: sensor[]) => {this.sensors = data;console.log(data)})
    this.getTheStationDetails();
  }

  deleteSensor(id){
    Swal.fire({
      title: 'Estas seguro?',
      text: "¡No podrás revertir esto!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, bórralo!'
    }).then((result) => {
      if (result.value) {
        this.packService.deleteSensor(id).subscribe(()=>console.log('deleted'));
        Swal.fire(
          '¡Eliminado!',
          'Su sensor ha sido eliminado.',
          'success'
        )
      }
    }).then(()=> window.location.reload());
  }

  getTheStationDetails(){
    this.packService.getOneStationById(this.idStation).subscribe((data)=>{
      this.station = data;
      console.log(data);
    })
  }

  editSensor(station , id, stationEn , idEn){
    this.packService.editSensor(station, id).subscribe(()=>{
      console.log('success');
      Swal.fire(
        'Modificado!',
        'Su sensor ha sido modificado.',
        'success'
      )
    });
    this.packService.editSensorEn(stationEn, idEn).subscribe(()=>{
      console.log('en success')
    })
  }

  getEnSensor(id){
    this.packService.getEnglishSensorFromEs(id).subscribe((data)=>{
      console.log(data);
      this.sensorEn = data;
    });
  }

}
