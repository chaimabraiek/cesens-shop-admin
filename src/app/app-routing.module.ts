import { NgModule } from '@angular/core';

import {RouterModule, Routes} from '@angular/router';
import {AddPackComponent} from './add-pack/add-pack.component';
import {AddStationComponent} from './add-station/add-station.component';
import {ListPacksComponent} from './list-packs/list-packs.component';
import {ListSensorsComponent} from './list-sensors/list-sensors.component';

const routes: Routes = [
  { path: 'pack', component: AddPackComponent },
  { path: 'station', component: AddStationComponent},
  { path: 'all', component: ListPacksComponent},
  { path: 'sensors/:id', component: ListSensorsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
